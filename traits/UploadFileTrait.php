<?php

namespace magisterapp\traits;

use yii\helpers\Inflector;
use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\helpers\FileHelper;


/**
 * UploadFileTrait funciona para centralizar la carga de atributos que deban ser cargados al servidor con \yii\web\UploadedFile
 *
 * @package magisterapp
 * @subpackage traits
 * @category Traits
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.comn.co>
 * @copyright Copyright (c) 2020 MagisterApp S.A.S.
 * @version 1.0.0
 * @since 1.7.0
 */
trait UploadFileTrait
{

    /**
     * Entrega los atributos que tienen que ser manipulados para la carga
     *
     * @var array|string
     */
    abstract public function getAttributesToUpload();

    /**
     * Acctiones a realizar despues de hacer busqueda de un registro Active Record
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->setUploadAttributes();
    }

    /**
     * Método encargado de inicializar con la url los atributos que sean de carga
     *
     * @return void
     */
    protected function setUploadAttributes()
    {
        $attributes = $this->getAttributesToUpload();

        if (is_string($attributes)) {
            $attributes = [$attributes];
        }

        foreach ($attributes as $attribute) {
            if ($this->$attribute && is_file($this->getPathUploads() . $this->$attribute)) {
                $this->$attribute = $this->getUrlUploads(true) . $this->$attribute;
            } else {
                $this->$attribute = null;
            }
        }
    }

    /**
     * Método encargado de cargar la imagen
     *
     * @param boolean $time Indica si se debe agregar el time al nombre de archivo o no
     * @return boolean
     */
    public function upload($time = true)
    {
        $attributes = $this->getAttributesToUpload();
        $cont = 0;

        if (is_string($attributes)) {
            $attributes = [$attributes];
        }

        foreach ($attributes as $attribute) {
            if ($this->$attribute instanceof UploadedFile) {
                $fileName = $attribute .  ($time ? '_' . time() : '') . '.' . $this->$attribute->extension;

                if ($this->$attribute->saveAs($this->getPathUploads() . $fileName)) {
                    if (is_file($this->getPathUploads() . $this->getOldAttribute($attribute))) {
                        unlink($this->getPathUploads() . $this->getOldAttribute($attribute));
                    }
                    $this->$attribute = $fileName;
                    $this->update(false, [$attribute]);
                    $this->$attribute = $this->getUrlUploads() . $fileName;
                }
                $cont++;
            }
        }

        return $cont == count($attributes);
    }

    
    /**
     * Método encargado de entregar el nombre neto del archivo de un atributo
     *
     * @param string $attribute Atributo a verificar
     * @return string
     */
    public function getOnlyFileName($attribute)
    {
        $fileName = explode('/', $this->$attribute);

        return $fileName[count($fileName) - 1];
    }

    /**
     * Métoco encargado de entregar la ruta base para carga de los archivos
     *
     * @return string
     */
    public function getRoute()
    {
        $idTableName = Inflector::camel2id(static::tableName());
        $primaryKey = static::primaryKey();

        if (is_array($primaryKey)) {
            $primaryKey = $primaryKey[0];
        }

        return "/uploads/{$idTableName}/{$this->$primaryKey}/";
    }

    /**
     * Devuelve la ruta fisica en el servidor para la carga de archivos
     * @return string
     */
    public function getPathUploads()
    {
        $path = Yii::getAlias('@webroot') . $this->getRoute();

        FileHelper::createDirectory($path);

        return $path;
    }

    /**
     * Devuelve la Url para la carga de archivos del servidor
     *
     * @param boolean $full indica si la ruta es completa o no
     * @return string
     */
    public function getUrlUploads($full = true)
    {
        return Url::base($full) .  $this->getRoute();
    }
}
