<?php

namespace magisterapp\web;

use yii\web\AssetBundle;

/**
 * Esta Clase Administra los Assets para el plugin bootbox.js
 * @package magisterapp
 * @subpackage assets
 * @category Assets
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class BaseAsset extends AssetBundle
{

    /**
     * @var string
     * Source base para el Asset
     */
    public $sourcePath = '@vendor/magisterapp/yii2-base/assets';

    /**
     * @var array
     * Archivos CSS
     */
    public $css = [];

    /**
     * @var array
     * Archivos JavaScript
     */
    public $js = [
        'base.js'
    ];

    /**
     * @var array
     * Dependencias del Asset
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'magisterapp\web\BootBoxAsset',
    ];

}
