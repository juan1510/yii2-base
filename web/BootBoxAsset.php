<?php

namespace magisterapp\web;

use yii\web\AssetBundle;

/**
 * Esta Clase Administra los Assets para el plugin bootbox.js
 * @package magisterapp
 * @subpackage assets
 * @category Assets
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class BootBoxAsset extends AssetBundle
{

    /**
     * @var string
     * Source base para el Asset
     */
    public $sourcePath = '@bower/bootbox.js/dist';

    /**
     * @var array
     * Archivos CSS
     */
    public $css = [];

    /**
     * @var array
     * Archivos JavaScript
     */
    public $js = [
        'bootbox.all.min.js'
    ];

    /**
     * @var array
     * Dependencias del Asset
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
