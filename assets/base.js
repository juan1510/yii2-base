/**
 * Inicializa Eventos
 */

$(function () {
    yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function (message, success) {
        bootbox.confirm(message, function (confirmed) {
            if (confirmed) {
                success();
            }
        });
        // confirm will always return false on the first call
        // to cancel click handler
        return false;
    }

    $('.breadcrumb li:first-child a').prepend("<i class='fa fa-home'></i> ");
    $('form[method="post"]').submit(function () {
        if (!this.id) {
            return false;
        }
        var el = $('#' + this.id);
        var form = $(this);
        setTimeout(function () {
            el = el.find('.has-error:first');
            setTimeout(function () {
                el.scrollTop();
                el.focus();
            }, 100);
            // if (el.length == 0 && form.attr('target') != '_blank') {
            //     showSpinner();
            // }
        }, 2000);
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

/**
 * Método para mostrar un mensaje de success
 * @param {String} msg
 */
function showSuccessMessage(msg) {
    getMessage('success', msg);
}

/**
 * Función para mostrar un mensaje de error
 * @param {String} msg
 */
function showErrorMessage(msg) {
    getMessage('error', msg);
}


/**
 * Función para mostrar un mensaje de info
 * @param {String} msg
 */
function showInfoMessage(msg) {
    getMessage('info', msg);
}

/**
 * Función para mostrar un mensaje
 * @param {String} type
 * @param {String} message
 */
function getMessage(type, message) {

    toastr[type]('<b>' + message + '</b>');
}

/**
 * Función para usar dentro de los eventos change para combos dependientes
 *
 * @param {Object} padre
 * @param {Object} hijo
 * @param {String} url
 * @param {Object} params
 * @param {Function} callBack
 */
function onChangeCombo(padre, hijo, url, params, callBack) {
    if (!params) {
        params = {
            id: padre.val()
        };
    }

    $.getJSON(url, params,
        function (data) {
            changeCombo(data, hijo, callBack);
        });
}

/**
 * Encargado de rellenar un select con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeCombo(data, hijo, callBack) {
    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var value in data) {
        var name = data[value];
        hijo.append("<option value='" + value + "'>" + name + "</option>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Encargado de rellenar un select agrupado con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeComboGroup(data, hijo, callBack) {

    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var label in data) {
        var dataItems = data[label];
        hijo.append("<optgroup  label='" + label + "'>");
        for (var value in dataItems) {
            var name = dataItems[value];
            hijo.append("<option value='" + value + "'>" + name + "</option>");
        }
        hijo.append("</optgroup>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Función encargada de retornar el valor de un parámetro url
 * @param {String} name
 * @returns {String}
 */
function getParamUrl(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(window.location);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Función encargada de abrir las modales de CRUD con opción de modal
 * @param {Object} button
 * @param {String} dynaModalId
 * @param {String} template
 */
function openModalGrid(button, dynaModalId, template) {
    let modalId = '#' + dynaModalId + '-modal';
    let url = $(button).attr('href');

    if (template == 'create') {
        $(modalId).find('.modal-title').html('Nuevo');
    } else if (template == 'update') {
        $(modalId).find('.modal-title').html('Editar');
    }

    if (template == 'view') {
        $(modalId).find('.modal-title').html('Ver');
        $(modalId).find('.modal-footer').hide();
    } else {
        $(modalId).find('.modal-footer').show();
    }

    $(modalId).find('.modal-body').load(url, function () {
        $(modalId).modal('show');

        $(modalId + ' [data-toggle="tooltip"]').tooltip();
        $(modalId + ' [data-toggle="popover"]').popover();

        $(modalId).find('.modal-body form').on('beforeSubmit', function (event, jqXHR, settings) {
            let form = $(this);
            let formData = form.serialize();
            let urlSplit = url.split('/');
            let dataAjax = {
                url: form.attr('action'),
                type: 'POST',
                beforeSend: function () {
                    $(modalId).find('button[type="submit"]').prop('disabled', true);
                },
                success: function (data) {
                    $(modalId).modal('hide');
                    $(this).off('beforeSubmit');
                    $(modalId).find('button[type="submit"]').prop('disabled', false);

                    if (data.state == 'success') {
                        showSuccessMessage(data.message);
                        $.pjax.reload({
                            container: '#gridview-' + dynaModalId + '-pjax',
                            url: window.location,
                            push: false,
                            replaceRedirect: false
                        })
                    } else {
                        showErrorMessage(data.message);
                    }

                }
            };

            if ($('input[type=file]').length > 0) {
                formData = new FormData(this);
                dataAjax.processData = false;
                dataAjax.contentType = false;
            }

            dataAjax.data = formData;

            $.ajax(dataAjax);
            return false;
        });
    });

}
