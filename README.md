Yii2 Base
=========
Componentes, Helpers y Widgets base para proyectos de MagisterApp S.A.S.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist magisterapp/yii2-generators "*"
```

or add

```
"magisterapp/yii2-generators": "*"
```

to the require section of your `composer.json` file.
