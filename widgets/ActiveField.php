<?php

namespace magisterapp\widgets;

use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveField as BaseActiveField;

/**
 * ActiveField representa un input de form para el [[ActiveForm]].
 *
 * @package magisterapp
 * @subpackage widgets
 * @category Widgets
 *
 * @property string $help Título para texto de ayuda para el tooltip
 * @property mixed $popover Texto completo de la ayuda
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveField extends BaseActiveField
{

    public $help         = null;
    public $popover;
    public $errorOptions = ['class' => 'invalid-feedback'];

    /**
     * {@inheritdoc}
     */
    public function label($label = null, $options = [])
    {
        if ($label === false)
        {
            $this->parts['{label}'] = '';
            return $this;
        }

        $options = array_merge($this->labelOptions, $options);
        if ($label !== null)
        {
            $options['label'] = $label;
        }
        $help = '';
        if (!is_null($this->help))
        {
            $help .= '&nbsp;&nbsp;&nbsp;&nbsp;' . Html::tag(
                            'span', $this->help,
                            [
                        'style' => 'color:#BFBFBF'
                            ]
            );
            if (!empty($this->popover))
            {
                $help .= '&nbsp;&nbsp;' . Html::tag(
                                'span', '',
                                [
                            'class'          => 'fa fa-info-circle',
                            'data-title'     => Yii::t('app', 'Ayuda'),
                            'data-placement' => 'top',
                            'data-content'   => $this->popover,
                            'data-trigger'   => 'hover',
                            'data-toggle'    => 'popover',
                                ]
                );
            }
        }
        $this->parts['{label}'] = Html::activeLabel(
                        $this->model, $this->attribute, $options
                ) . $help;

        return $this;
    }

}
