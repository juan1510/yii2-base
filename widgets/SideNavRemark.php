<?php

namespace magisterapp\widgets;

use Yii;
use magisterapp\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Este widget extiende yii\bootstrap\Nav para que se adapte a la plantilla
 *
 * En el parametro "icon" usar iconos de FontAwesome
 * Se puede usar el parametro "activeWhitController" para que este activo con
 * cualquier acción que pertenezca al controlador de la ruta del item (por defecto es "true")
 * @package magisterapp
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018-2020 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.1.0
 */
class SideNavRemark extends \yii\bootstrap\Nav
{

    /**
     * @var boolean
     * Indica si el menú debe ser oscuro o claro
     */
    public $dark = true;

    /**
     * @var array
     * Indica las opciones ubicadas en el footer del menú
     */
    public $footer = [];

    /**
     * Inicializa el widget
     */
    public function init()
    {
        parent::init();

        Html::removeCssClass($this->options, 'nav');
        Html::addCssClass($this->options, 'site-menu');
        $this->options['data-plugin'] = 'menu';
    }

    /**
     * Renders widget items.
     */
    public function renderItems()
    {
        $items = parent::renderItems();
        $footer = $this->renderFooter();

        return '
        <div class="site-menubar ' . (!$this->dark ? 'site-menubar-light' : '') . '">
            <div class="site-menubar-body scrollable" style="position: relative;">
                <div class="scrollable-container">
                    <div class="scrollable-content">' . $items . '</div>
                </div>
            </div>
            ' . $footer . '
        </div>';
    }


    /**
     * Método encargado de entregar la configuración del footer de menú
     *
     * @return string
     */
    public function renderFooter()
    {
        $foot = '';

        if (!empty($this->footer)) {

            $items = '';
            foreach ($this->footer as $footer) {
                $label = ArrayHelper::getValue($footer, 'label', '');
                $icon  = ArrayHelper::getValue($footer, 'icon', '');
                $url   = ArrayHelper::getValue($footer, 'url', 'javascript: void(0);');
                $options     = ArrayHelper::getValue($footer, 'options', []);

                $options['data-placement'] = 'top';
                $options['data-toggle'] = 'tooltip';
                $options['data-original-title'] = $label;

                $items .= Html::a(
                    '<span class="icon ' . $icon . '" aria-hidden="true"></span>',
                    $url,
                    $options
                );
            }

            $foot = Html::tag('div', $items, ['class' => 'site-menubar-footer']);
        }

        return $foot;
    }

    /**
     * Renders un item del widget
     * @param string|array $item el item que va a render.
     * @param boolean $sub Indica si es un subItem o no
     * @return string el resultado del render.
     * @throws InvalidConfigException
     */
    public function renderItem($item, $sub = false)
    {
        if (is_string($item)) {
            return Html::tag(
                'li',
                $item,
                ['class' => 'site-menu-category']
            );
        }

        if (!isset($item['label'])) {
            throw new InvalidConfigException("La opción 'label' es obligatoria.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $icon        = ArrayHelper::getValue($item, 'icon');
        $label       = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $label       = Html::tag(
            'i',
            '',
            ['class' => "site-menu-icon fa-{$icon}"]
        ) . Html::tag(
            'span',
            $label,
            [
                'class' => 'site-menu-title'
            ]
        );
        $badge     = ArrayHelper::getValue($item, 'badge', []);
        $options     = ArrayHelper::getValue($item, 'options', []);
        $items       = ArrayHelper::getValue($item, 'items');
        $url         = ArrayHelper::getValue($item, 'url', 'javascript:;');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $visible     = ArrayHelper::getValue($item, 'visible', true);

        if (!$visible) {
            return '';
        }

        if (!empty($badge)) {
            $badgeLabel     = ArrayHelper::getValue($badge, 'label', '');
            $badgeType     = ArrayHelper::getValue($badge, 'type', 'info');

            $label .= Html::tag(
                'div',
                Html::tag('span', $badgeLabel, ['class' => "badge badge-{$badgeType} badge-round"]),
                ['class' => 'site-menu-label']
            );
        }

        if (isset($item['url'])) {
            Html::addCssClass($options, 'site-menu-item');
        } else {
            Html::addCssClass($options, 'site-menu-category open');
        }

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if ($items !== null) {
            Html::addCssClass($options, ['widget' => 'has-sub']);
            $label .= Html::tag('span', '', ['class' => 'site-menu-arrow']);

            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }

                if ($this->hasChildActive($items)) {
                    Html::addCssClass($options, 'open');
                    Html::addCssClass($options, 'active');
                }

                $items = $this->renderTreeView($items, $item);
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'open');
            Html::addCssClass($options, 'active');
            Html::addCssClass($linkOptions, 'active');
        }

        return Html::tag(
            'li',
            Html::a($label, $url, $linkOptions) . $items,
            $options
        );
    }

    /**
     * Sobreescribe método para activarlo tambien cuando
     * este dentro de cualquier accion del controlador
     * @param array $item
     */
    protected function isItemActive($item)
    {
        $activeWhitController = ArrayHelper::getValue(
            $item,
            'activeWhitController',
            true
        );
        $active               = parent::isItemActive($item);

        if (is_array($item) && ($activeWhitController || $active)) {
            $route = explode('/', $item['url'][0]);
            if (isset($route[1])) {
                $active = $active || $route[1] === Yii::$app->controller->id;
            }
        }

        return $active;
    }

    /**
     * Indica si algun item hijo esta activo
     * @param array $items Items Hijos
     * @return boolean
     */
    protected function hasChildActive($items)
    {
        foreach ($items as $item) {
            if ($this->isItemActive($item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Renderiza los items para sub-menus
     * @param array $items items dados
     * @param array $parentItem item Padre
     * @return string Resultado Html
     */
    protected function renderTreeView($items, $parentItem)
    {
        Html::addCssClass($parentItem['options'], 'site-menu-sub');
        $content = '';

        foreach ($items as $item) {
            $content .= $this->renderItem($item, true);
        }

        return Html::tag('ul', $content, $parentItem['options']);
    }
}
