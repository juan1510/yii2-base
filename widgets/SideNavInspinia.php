<?php

namespace magisterapp\widgets;

use magisterapp\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Este widget extiende yii\bootstrap\Nav para que se adapte a la plantilla Inspinia
 *
 * En el parametro "icon" usar iconos de FontAwesome
 * Se puede usar el parametro "activeWhitController" para que este activo con
 * cualquier acción que pertenezca al controlador de la ruta del item (por defecto es "true")
 * @package magisterapp
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2020 MagisterApp S.A.S.
 * @version 1.0.1
 * @since 1.0.0
 */
class SideNavInspinia extends \yii\bootstrap\Nav
{

    /**
     * Inicializa el widget
     */
    public function init()
    {
        parent::init();

        Html::addCssClass($this->options, 'nav metismenu');
        $this->options['id'] = 'side-menu';
    }

    /**
     * Renders un item del widget
     * @param string|array $item el item que va a render.
     * @return string el resultado del render.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("La opción 'label' es obligatoria.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $icon = ArrayHelper::getValue($item, 'icon');
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $label = Html::iconFontAwesome($icon) . Html::tag('span', $label, ['class' => 'nav-label']);
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $visible = ArrayHelper::getValue($item, 'visible', true);
        $badge = ArrayHelper::getValue($item, 'badge', NULL);
        $badgeType = ArrayHelper::getValue($item, 'badgeType', 'label-primary');

        $options['aria-expanded'] = 'false';

        if (!$visible) {
            return '';
        }

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if (!is_null($badge)) {
            $label .= Html::tag('span', $badge, ['class' => "float-right label {$badgeType}"]);
        }

        if ($items !== null) {
            $label .= Html::tag('span', '', ['class' => 'fa arrow']);

            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }

                if ($this->hasChildActive($items)) {
                    Html::addCssClass($options, 'active');
                }

                $items = $this->renderTreeView($items, $item);
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag(
            'li',
            Html::a($label, $url, $linkOptions) . $items,
            $options
        );
    }

    /**
     * Sobreescribe método para activarlo tambien cuando
     * este dentro de cualquier accion del controlador
     * @param array $item
     */
    protected function isItemActive($item)
    {
        $activeWhitController = ArrayHelper::getValue(
            $item,
            'activeWhitController',
            true
        );
        $url = ArrayHelper::getValue(
            $item,
            'url',
            'javascript:;'
        );
        $active = parent::isItemActive($item);

        if (($activeWhitController || $active) && is_array($url)) {
            $route = explode('/', $url[0]);

            if (isset($route[1])) {
                $active = $active || $route[1] === Yii::$app->controller->id || $route[1] === Yii::$app->controller->module->id;
            }
        } elseif (Yii::$app->controller->module->id !== Yii::$app->id) {
            $route = explode('/', $url[0]);
            $controller = isset($route[1]) ? $route[1] : NULL;
            $action = isset($route[2]) ? $route[2] : NULL;

            if ($controller && $action) {
                $active = $active || "{$controller}/{$action}" == Yii::$app->controller->module->id . "/" . Yii::$app->controller->id;
            }
        }

        return $active;
    }

    /**
     * Indica si algun item hijo esta activo
     * @param array $items Items Hijos
     * @since 1.0.8
     * @return boolean
     */
    protected function hasChildActive($items)
    {
        foreach ($items as $item) {

            if ($this->isItemActive($item)) {
                if (isset($item['items']) && count($item['items']) > 0) {
                    if ($this->hasChildActive($item['items'])) {
                        return true;
                    }
                } else {
                    return true;
                }
            } elseif (isset($item['items']) && count($item['items']) > 0) {
                if ($this->hasChildActive($item['items'])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Renderiza los items para sub-menus
     * @param array $items items dados
     * @param array $parentItem item Padre
     * @return string Resultado Html
     */
    protected function renderTreeView($items, $parentItem)
    {
        Html::addCssClass($parentItem['options'], 'nav nav-second-level collapse');
        $parentItem['options']['aria-expanded'] = 'false';
        $content = '';

        foreach ($items as $item) {
            $content .= $this->renderItem($item);
        }

        return Html::tag('ul', $content, $parentItem['options']);
    }
}
