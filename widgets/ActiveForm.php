<?php

namespace magisterapp\widgets;

use yii\bootstrap\ActiveForm as BaseActiveForm;

/**
 * ActiveFom es un widget que construye un formulario HTML interactivo con uno o varios modelos.
 *
 * @package magisterapp
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveForm extends BaseActiveForm
{

    /**
     * @var string Using different style themes:
     *
     * + default: Default layout with labels at the top of the fields.
     * + horizontal: Horizontal layout set the labels left to the fields.
     */
    public $layout = 'default';

    /**
     * @var string The error Summary alert class
     */
    public $errorSummaryCssClass = 'error-summary alert alert-danger';

    /**
     * @inheritdoc
     */
    public $errorCssClass = 'is-invalid';

    /**
     * @var string Validation type
     */
    public $validationStateOn = self::VALIDATION_STATE_ON_INPUT;

    /**
     * @inheritdoc
     */
    public $successCssClass = 'is-valid';

    /**
     * @inheritdoc
     */
    public $fieldClass = 'magisterapp\widgets\ActiveField';

}
