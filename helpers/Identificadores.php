<?php

namespace magisterapp\helpers;

/**
 * Permite la facil administración de los Identificadores de registros base del sistema
 *
 * @package magisterapp
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Identificadores
{

}
