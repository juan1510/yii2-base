<?php

namespace magisterapp\base;

use magisterapp\helpers\Html;
use Yii;
use yii\web\Controller as ControllerBase;
use yii\filters\VerbFilter;
use yii\web\Response;
use magisterapp\helpers\UI;
use magisterapp\helpers\Message;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Controlador base para todas las CRUD generales
 *
 * @package magisterapp
 * @subpackage base
 * @category Base
 *
 * @property string $searchModel Ruta del modelo para la búsqueda.
 * @property string $model Ruta del modelo principal.
 * @property string $layout PATH del layout utilizado por defecto.
 *
 * @author  Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.1.0
 * @since 1.0.0
 */
class Controller extends ControllerBase
{

    public $searchModel = 'app\models\searchs';
    public $model = 'app\models';
    public $layout = '@theme/views/layouts/main';
    public $actionRedirectCreate = 'view';
    public $actionRedirectUpdate = 'view';
    public $actionRedirectDelete = ['index'];
    public $actionRedirectRestore = ['index'];

    /**
     * Método para sobreescribir comportamientos antes de las acciones
     *
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        Yii::$container->set(
            'kartik\dynagrid\DynaGrid',
            UI::getDefaultConfigDynaGrid()
        );
        Yii::$container->set(
            'kartik\grid\GridView',
            UI::getDefaultConfigGridView()
        );
        Yii::$container->set(
            'kartik\widgets\TimePicker',
            UI::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\time\TimePicker',
            UI::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DatePicker',
            UI::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\date\DatePicker',
            UI::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DateTimePicker',
            UI::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\datetime\DateTimePicker',
            UI::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\Select2',
            UI::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\select2\Select2',
            UI::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\dialog\Dialog',
            UI::getDefaultConfigDialog()
        );

        Yii::$app->session->open();
        // if ($action->id == 'error')
        // {
        //     $this->layout = 'main_blank';
        // }


        return parent::beforeAction($action);
    }

    /**
     * Configuración de comportamientos
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'restore' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->searchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $isAjax = false;

        if (Yii::$app->request->isAjax && Yii::$app->request->get('render-ajax')) {
            $isAjax = true;
        }

        return $isAjax  ? $this->renderAjax(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ) : $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }


    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new $this->model();

        foreach (Yii::$app->request->get() as $param => $value) {
            if ($model->hasAttribute($param)) {
                $model->{$param} = $value;
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $attributesToUpload = $this->getAttributesToUpload($model);

            foreach ($attributesToUpload as $attribute) {
                $model->$attribute = UploadedFile::getInstance($model, $attribute);
            }

            if ($model->save()) {

                if (count($attributesToUpload) > 0) {
                    $model->upload();
                }

                Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se creó con éxito.'));

                return $this->redirect([$this->actionRedirectCreate, 'id' => $model->primaryKey]);
            } else {
                Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar crear.') . '<br>' . trim(preg_replace('/\s+/', ' ', Html::errorSummary($model))));
            }
        }

        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $attributesToUpload = $this->getAttributesToUpload($model);

            foreach ($attributesToUpload as $attribute) {
                $model->$attribute = $model->getOldAttribute($attribute);
            }

            if ($model->save()) {

                if (count($attributesToUpload) > 0) {
                    foreach ($attributesToUpload as $attribute) {
                        $model->$attribute = UploadedFile::getInstance($model, $attribute);
                    }
                    $model->upload();
                }

                Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se editó con éxito.'));

                return $this->redirect([$this->actionRedirectUpdate, 'id' => $model->primaryKey]);
            } else {
                Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar editar.') . '<br>' . trim(preg_replace('/\s+/', ' ', Html::errorSummary($model))));
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite realizar el borrado lógico del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->{$this->model::STATUS_COLUMN} = $this->model::STATUS_INACTIVE;

        if ($model->save(false, [$this->model::STATUS_COLUMN])) {
            Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se eliminó con éxito.'));
        } else {
            Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar eliminar.'));
        }

        return $this->redirect($this->actionRedirectDelete);
    }

    /**
     * Permite realizar la restauración del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->{$this->model::STATUS_COLUMN} = $this->model::STATUS_ACTIVE;

        if ($model->save(false, [$this->model::STATUS_COLUMN])) {
            Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se restauró con éxito.'));
        } else {
            Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar restaurar.'));
        }

        return $this->redirect($this->actionRedirectRestore);
    }

    /**
     * Busca un registro basado en su llave primaria.
     * Si el registro no existe arroja un error HTTP 404.
     *
     * @param integer $id
     * @return Model con el registro.
     * @throws NotFoundHttpException is el registro no es encontrado.
     */
    protected function findModel($id)
    {
        if (($model = $this->model::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t(
                'app',
                'La página solicitada no existe.'
            ));
        }
    }

    /**
     * Método encargado de verificar y entregar los attributos de un modelo que deban ser instanciados por UploadedFile
     *
     * @param Model $model
     * @return array
     */
    protected function getAttributesToUpload(Model $model)
    {
        $attributes = [];

        if (method_exists($model, 'getAttributesToUpload')) {
            $attributes = $model->getAttributesToUpload();

            if (is_string($attributes)) {
                $attributes = [$attributes];
            }
        }

        return $attributes;
    }
}
