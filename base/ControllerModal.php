<?php

namespace magisterapp\base;

use ReflectionClass;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * Controlador base para las CRUD con implementación en ventanas modales
 *
 * @package magisterapp
 * @subpackage base
 * @category Base
 *
 * @property string $searchModel Ruta del modelo para la búsqueda.
 * @property string $model Ruta del modelo principal.
 * @property string $layout PATH del layout utilizado por defecto.
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.com.co>
 * @copyright Copyright (c) 2018 MagisterApp S.A.S.
 * @version 0.1.0
 * @since 1.0.0
 */
class ControllerModal extends Controller
{

    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->renderAjax(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new $this->model();

        foreach (Yii::$app->request->get() as $param => $value) {
            if ($model->hasAttribute($param)) {
                $model->{$param} = $value;
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $attributesToUpload = $this->getAttributesToUpload($model);

            foreach ($attributesToUpload as $attribute) {
                $model->$attribute = UploadedFile::getInstance($model, $attribute);
            }

            if ($model->save()) {
                if (count($attributesToUpload) > 0) {
                    $model->upload();
                }

                $res['state'] = 'success';
                $res['message'] = Yii::t('app', 'Se creó con éxito.');
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($model);
                $res['error'] = ActiveForm::validate($model);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_form',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $attributesToUpload = $this->getAttributesToUpload($model);

            foreach ($attributesToUpload as $attribute) {
                $model->$attribute = $model->getOldAttribute($attribute);
            }

            if ($model->save()) {

                if (count($attributesToUpload) > 0) {
                    foreach ($attributesToUpload as $attribute) {
                        $model->$attribute = UploadedFile::getInstance($model, $attribute);
                    }
                    $model->upload();
                }

                $res['state'] = 'success';
                $res['message'] = Yii::t('app', 'Se editó con éxito.');
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($model);
                $res['error'] = ActiveForm::validate($model);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_form',
                [
                    'model' => $model,
                ]
            );
        }
    }
}
