<?php

namespace magisterapp\components;

use stdClass;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Permite la facil emsisión de mensages push con One Signal
 *
 * @package app
 * @subpackage components
 * @category Components
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.comn.co>
 * @copyright Copyright (c) 2020 Magister App S.A.S.
 * @version 0.0.1
 * @since 1.4.0
 */
class OneSignal extends Component
{

    const LIMIT_PLAYERS_ID = 2000;

    /**
     * @var string
     * Token de acceso One Signal
     */
    public $token;

    /**
     * @var string
     * Id de la aplicación One Signal
     */
    public $app;

    /**
     * Inicializa el componente
     */
    public function init()
    {
        parent::init();

        if (!$this->app || !$this->token) {
            new InvalidConfigException('Se debe definir el id  de app y el Rest Token de One Signal');
        }
    }

    /**
     * Método para hacer las llamadas HTTP a los metodos de One Signal
     * @param string $method Endpoint de One Signal a llamar
     * @param array $data Parámetros que se enviarán al método
     * @param array $type Método HTTP que se va a llamar
     * @return array
     */
    public function call($method, $data = [], $type = 'POST')
    {
        $fields = Json::encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/{$method}");
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json; charset=utf-8',
                "Authorization: Basic {$this->token}"
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        if ($type == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        return Json::decode($response);
    }

    /**
     * Método para enviar una notificación
     * @param string|array $hwids Identificadores de dispositivos a los cuales llegará la notificación
     * @param string|array $message Texto del Mensaje a enviar, se puede recibir un array multiidioma
     * @param array $data Data adicional que va a recibir la app
     * @param integer $badge Número que se desea colocar como badge dentro de la app en iOS (Opcional), por defecto incrementará el número que tenga
     * @param array $options Opciones adicionales que se quieran enviar al método Create Notification de One Signal
     * @return array
     */
    public function createMessage($hwids, $message, $data = [], $badge = NULL, $options = [])
    {
        $response = $this->call(
            'notifications',
            ArrayHelper::merge([
                'app_id' => $this->app,
                'include_player_ids' => is_array($hwids) ? $hwids : [$hwids],
                'data' => !empty($data) ? $data : new stdClass(),
                'ios_badgeType' => is_null($badge) ? 'Increase' : 'SetTo',
                'ios_badgeCount' => is_null($badge) ? 1 : $badge,
                'contents' => is_array($message) ? $message : ['es' => $message, 'en' => $message],
            ], $options)
        );
        return $response;
    }

    /**
     * Método para cancelar una notificación
     * @param string $id Identificador del push generado por One Signal
     * @return array
     */
    public function cancelMessage($id)
    {
        $response = $this->call(
            "notifications/{$id}?app_id={$this->app}",
            [],
            'DELETE'
        );

        return $response;
    }
}
