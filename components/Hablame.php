<?php

namespace magisterapp\components;

use stdClass;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Permite la facil emsisión de mensages sms con Hablame.com
 *
 * @package app
 * @subpackage components
 * @category Components
 *
 * @author Juan David Rodriguez Ramirez <juan.rodriguez@magisterapp.comn.co>
 * @copyright Copyright (c) 2020 Magister App S.A.S.
 * @version 0.0.1
 * @since 1.5.0
 */
class Hablame extends Component
{
    const BASE_URL_PRIMARY = 'https://api101.hablame.co/api/sms/v2.1/';
    const BASE_URL_BACKUP = 'https://api102.hablame.co/api/sms/v2.1/';

    /**
     * @var string
     * Numero de cuenta del cliente
     */
    public $account;

    /**
     * @var string
     * Api key del cliente
     */
    public $apiKey;

    /**
     * @var string
     * Token del cliente
     */
    public $token;


    /**
     * Inicializa el componente
     */
    public function init()
    {
        parent::init();

        if (!$this->account || !$this->apiKey || !$this->token) {
            new InvalidConfigException('Se deben definir los parámetros account, apiKey y token para el correcto funcionamiento del componente.');
        }
    }

    /**
     * Método para hacer las llamadas HTTP a los metodos de Hablame
     * @param string $method Endpoint de Hablame a llamar
     * @param array $data Parámetros que se enviarán al método
     * @param array $type Método HTTP que se va a llamar
     * @return array
     */
    public function call($method, $data = [], $type = 'POST')
    {
        $dataRequired = [
            'account' => $this->account,
            'apiKey' => $this->apiKey,
            'token' => $this->token,
        ];

        $fields = ArrayHelper::merge($dataRequired, $data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BASE_URL_PRIMARY . $method);

        if ($type == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $response = curl_exec($ch);

        if ($response === false) {
            curl_setopt($ch, CURLOPT_URL, self::BASE_URL_BACKUP . $method);
            $response = curl_exec($ch);
        }

        curl_close($ch);

        return Json::decode($response);
    }

    /**
     * Método para enviar un SMS
     * @param string $toNumber Número o Números a los cuales se enviará el SMS
     * @param string $sms Contenido del SMS
     * @param string $data Parámetros adicionales de Hablame
     * @param string $type Método HTTP que se usará para la petición
     * @return array
     */
    public function send($toNumber, $sms, $data = [], $type = 'POST')
    {

        return $this->call(
            "send/",
            ArrayHelper::merge([
                'toNumber' => $toNumber,
                'sms' => $sms
            ], $data),
            $type
        );
    }

    /**
     * Método para consultar un SMS por smsId
     * @param string $smsId Identificador del SMS en Hablame
     * @param string $type Método HTTP que se usará para la petición
     * @return array
     */
    public function querySms($smsId, $type = 'POST')
    {

        return $this->call(
            "query/smsId/",
            [
                'smsId' => $smsId
            ],
            $type
        );
    }

    /**
     * Método para consultar un SMS por loteId
     * @param string $loteId Identificador del SMS en Hablame
     * @param string $type Método HTTP que se usará para la petición
     * @return array
     */
    public function querylote($loteId, $type = 'POST')
    {

        return $this->call(
            "query/loteId/",
            [
                'loteId' => $loteId
            ],
            $type
        );
    }
}
